/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

import java.util.List;
import javax.swing.text.DefaultEditorKit;

/**
 *
 * @author Msio
 */
public class ArtistService {
    public List<ArtistReport> getArtistTopTenByTotalPrice() {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistTopTenByTotalPrice(10);
    }
    public List<ArtistReport> getArtistTopTenByTotalPrice(String begin ,String end) {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistTopTenByTotalPrice(begin,end,10);
    }
}
